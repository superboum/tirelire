const settle = (sums) => {
  const creditors = []
  const owers = []
  const transfers = []

  sums.forEach((v, i) => {
    if (v > 0) {
      creditors.push([i, v])
    } else if (v < 0) {
      owers.push([i, -1 * v])
    }
  })

  owers.sort((a, b) => b[1] - a[1])
  creditors.sort((a, b) => b[1] - a[1])

  while (owers.length) {
    const o = owers[0]
    const c = creditors[0]

    const balance = (o[1] - c[1]).toFixed(2)

    if (balance > 0) {
      creditors.shift()
      owers[0][1] = balance
      transfers.push([o[0], c[0], c[1]])
    } else {
      if (balance >= 0) {
        creditors.shift()
      } else {
        creditors[0][1] = -1 * balance
      }
      owers.shift()
      transfers.push([o[0], c[0], o[1]])
    }
  }

  return transfers
}

export default settle;
