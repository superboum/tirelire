package main

import (
	"encoding/json"
	"errors"
	"io"
	"sync"

	"github.com/dgraph-io/badger"
	"github.com/dgraph-io/badger/options"
	"golang.org/x/crypto/ed25519"
	"golang.org/x/crypto/nacl/sign"
)

const nonceLength = 24
const publicLength = 32

type operation struct {
	UUID   string `json:"uuid"`
	Nonce  []byte `json:"nonce"`
	Put    []byte `json:"put"`
	Delete []byte `json:"delete,omitempty"`
}

type mutation struct {
	pub    [ed25519.PublicKeySize]byte
	Public []byte      `json:"public"`
	Ops    []operation `json:"ops"`
}

func parseMutation(reader io.Reader) (m *mutation, err error) {
	m = &mutation{}
	decoder := json.NewDecoder(reader)
	err = decoder.Decode(&m)

	if err == nil {
		if len(m.Public) != ed25519.PublicKeySize {
			err = errors.New("invalid public key size")
		} else {
			copy(m.pub[:], m.Public)
		}
	}

	return
}

type store struct {
	sync.Mutex
	db *badger.DB
}

func openStore() (*store, error) {
	opts := badger.DefaultOptions("tirelire-store")
	opts.ValueLogLoadingMode = options.FileIO

	db, err := badger.Open(opts)
	if err != nil {
		return nil, err
	}

	return &store{db: db}, nil
}

func (s *store) ExecuteMutation(m *mutation) error {
	// We lock the store to avoid handling conflicting write.
	s.Lock()
	defer s.Unlock()

	tx := s.db.NewTransaction(true)

	for _, op := range m.Ops {
		if op.Put != nil {
			_, signed := sign.Open(nil, op.Put, &m.pub)
			if !signed || len(op.Nonce) != nonceLength {
				continue
			}

			key := append(m.Public, []byte(op.UUID)...)
			value := append(op.Nonce, op.Put...)
			_ = tx.Set(key, value)
		}
		if op.Delete != nil {
			expected := []byte(op.UUID)
			signed := ed25519.Verify(m.Public, expected, op.Delete)
			if !signed {
				continue
			}

			key := append(m.Public, []byte(op.UUID)...)
			_ = tx.Delete(key)
		}
	}

	return tx.Commit()
}

func (s *store) GetMutation(public []byte) (*mutation, error) {
	if len(public) != publicLength {
		return nil, errors.New("invalid public length")
	}

	tx := s.db.NewTransaction(false)
	defer tx.Discard()

	it := tx.NewIterator(badger.DefaultIteratorOptions)
	defer it.Close()

	m := &mutation{
		Public: public,
		Ops:    make([]operation, 0, 16),
	}

	for it.Seek(public); it.ValidForPrefix(public); it.Next() {
		item := it.Item()
		k := item.Key()
		v, err := item.ValueCopy(nil)
		if err != nil {
			return nil, err
		}

		m.Ops = append(m.Ops, operation{
			UUID:  string(k[publicLength:]),
			Nonce: v[:nonceLength],
			Put:   v[nonceLength:],
		})
	}

	return m, nil
}
